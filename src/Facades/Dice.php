<?php
/**
 * Project: Dice.
 * Author: Regin Thybo Lyngsø
 * Date: 10-06-2018
 * Time: 16:17
 */

namespace Serringer\Dice\Facades;

use Illuminate\Support\Facades\Facade;

class Dice extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Dice';
    }

}
<?php
/**
 * Created by PhpStorm.
 * Author: Regin Thybo Lyngsø
 * Date: 10-06-2018
 * Time: 12:39
 */

namespace Serringer\Dice;

use Illuminate\Support\ServiceProvider;

class DiceServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/serringer_dice.php' => config_path('serringer_dice.php'),
        ], 'dice-config');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/serringer_dice.php',
            'serrringer_dice'
        );

        $this->app->bind('Dice', function () {
            return new Dice();
        });
    }

}
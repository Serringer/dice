<?php
/**
 * Project: Serringer/Dice.
 * Author: Regin Thybo Lyngsø
 * Date: 10-06-2018
 * Time: 12:56
 */

namespace Serringer\Dice;
use Illuminate\Support\Collection;
use Illuminate\Support\Traits\Macroable;

class Dice
{
    const DICE_REGEX = '(?P<multiple>\d*)d(?P<dietype>\d+|f|\%|\[[^\]]+\])((?P<keep>k(?:eep)?(?P<keepeval>[<>])(?P<keeprange>\d+))|(?P<lowest>l(?:owest)?(?P<lowdice>\d+))|(?P<highest>h(?:ighest)?(?P<highdice>\d+))|(?P<reroll>r(?:eroll)?(?P<rerolleval>[<>])(?P<rerolllimit>\d+))|(?P<openroll>o(?:pen)?(?P<openrolleval>[<>=])(?P<openrolllimit>\d+))|(?P<flags>[z]+))*';

    private $ooo = ['>' => 0, '<' => 0, '=' => 0, '-' => 10, '+' => 10, '*' => 20, '/' => 20, '^' => 30,];

    protected $diceTower;
    protected $rpn = [];
    protected $infix = [];
    protected $stack = [];


    public function __construct($diceTower = '')
    {
        $this->diceTower = str_replace(' ', '', $diceTower);

        preg_match_all('%(?:
            (?P<dice>' . self::DICE_REGEX . ')
            |
            (?P<set>\d*\[[^\]]+\])
            |
            (?P<numeral>[\d\.]+)
            |
            (?P<operator>[+\-*^><=/])
            |
            (?P<variable>\$[a-z_]+)
            |
            (?P<parens>[()])
        )%ix', $this->diceTower, $matches, PREG_SET_ORDER);

        $this->stack = [];

        foreach ($matches as $match) {
            $match = array_filter($match, function ($value) {
                return $value !== false && $value !== '';
            });

            dd($match);
        }
    }

}